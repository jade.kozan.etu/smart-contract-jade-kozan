pragma solidity ^0.8.20;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract Voting is Ownable {
    mapping(address => Voter) public voters;
    Proposal[] public proposals;
    WorkflowStatus public workflowStatus;

    IERC20 public votingToken;

    struct Voter {
        bool isRegistered;
        bool hasVoted;
        uint votedProposalId;
        uint voteWeight;
        address delegate;
        uint[] voteHistory;
    }

    struct Proposal {
        string description;
        uint voteCount;
    }

    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesTallied
    }

    event VoterRegistered(address voterAddress, uint voteWeight);
    event WorkflowStatusChange(
        WorkflowStatus previousStatus,
        WorkflowStatus newStatus
    );
    event ProposalRegistered(uint proposalId);
    event Voted(address voter, uint proposalId, uint voteWeight);

    constructor(address _votingToken) {
        votingToken = IERC20(_votingToken);
        workflowStatus = WorkflowStatus.RegisteringVoters;
    }

    function registerVoter(address _voterAddress, uint _voteWeight) public onlyOwner {
        require(
            workflowStatus == WorkflowStatus.RegisteringVoters,
            "Impossible d'enregistrer un électeur."
        );
        require(
            !voters[_voterAddress].isRegistered,
            "Électeur déjà enregistré."
        );

        voters[_voterAddress] = Voter({
            isRegistered: true,
            hasVoted: false,
            votedProposalId: 0,
            voteWeight: _voteWeight,
            delegate: address(0),
            voteHistory: new uint[](0)
        });

        emit VoterRegistered(_voterAddress, _voteWeight);
    }

    function delegateVote(address _delegate) public {
        require(voters[msg.sender].isRegistered, "Seuls les électeurs enregistrés peuvent déléguer leur vote.");
        require(voters[msg.sender].delegate != msg.sender, "Vous ne pouvez pas déléguer votre vote à vous-même.");
        voters[msg.sender].delegate = _delegate;
    }

    function startProposalsRegistration() public onlyOwner {
        require(
            workflowStatus == WorkflowStatus.RegisteringVoters,
            "Impossible d'enregistrer les propositions"
        );

        workflowStatus = WorkflowStatus.ProposalsRegistrationStarted;

        emit WorkflowStatusChange(
            WorkflowStatus.RegisteringVoters,
            workflowStatus
        );
    }

    function registerProposal(string memory _description) public {
        require(
            workflowStatus == WorkflowStatus.ProposalsRegistrationStarted,
            "Impossible d'enregistrer des propositions."
        );
        require(
            voters[msg.sender].isRegistered,
            "Les électeurs enregistrés SEULEMENT peuvent soumettre des propositions."
        );

        proposals.push(Proposal({description: _description, voteCount: 0}));

        emit ProposalRegistered(proposals.length - 1);
    }

    function startVotingSession() public onlyOwner {
        require(
            workflowStatus == WorkflowStatus.ProposalsRegistrationEnded,
            "Impossible de commencer la session de vote."
        );

        workflowStatus = WorkflowStatus.VotingSessionStarted;

        emit WorkflowStatusChange(
            WorkflowStatus.ProposalsRegistrationEnded,
            workflowStatus
        );
    }

    function vote(uint _proposalId) public {
        require(
            workflowStatus == WorkflowStatus.VotingSessionStarted,
            "Impossible de voter"
        );
        require(
            voters[msg.sender].isRegistered,
            "Les électeurs enregistrés SEULEMENT peuvent voter."
        );
        require(!voters[msg.sender].hasVoted, "L'électeur a déjà voté.");
        require(_proposalId < proposals.length, "ID incorrect");

        address delegate = voters[msg.sender].delegate;
        if (delegate != address(0)) {
            vote(delegate, _proposalId);
        } else {
            vote(msg.sender, _proposalId);
        }
    }

    function vote(address _voter, uint _proposalId) internal {
        require(voters[_voter].isRegistered, "L'électeur doit être enregistré.");
        require(!voters[_voter].hasVoted, "L'électeur a déjà voté.");
        require(_proposalId < proposals.length, "ID incorrect");

        voters[_voter].hasVoted = true;
        voters[_voter].votedProposalId = _proposalId;

        uint weightedVoteCount = voters[_voter].voteWeight;
        proposals[_proposalId].voteCount += weightedVoteCount;

        voters[_voter].voteHistory.push(_proposalId);

        emit Voted(_voter, _proposalId, weightedVoteCount);
    }

    function endVotingSession() public onlyOwner {
        require(
            workflowStatus == WorkflowStatus.VotingSessionStarted,
            "Impossible de terminer la session de vote."
        );

        workflowStatus = WorkflowStatus.VotesTallied;

        emit WorkflowStatusChange(
            WorkflowStatus.VotingSessionEnded,
            workflowStatus
        );
    }

    function getWinner() public view returns (uint winningProposalId) {
        require(
            workflowStatus == WorkflowStatus.VotesTallied,
            "Impossible d'avoir le gagnant"
        );

        uint winningVoteCount = 0;
        for (uint i = 0; i < proposals.length; i++) {
            if (proposals[i].voteCount > winningVoteCount) {
                winningVoteCount = proposals[i].voteCount;
                winningProposalId = i;
            }
        }

        return winningProposalId;
    }

    function getVoteHistory(address _voter) public view returns (uint[] memory) {
        require(voters[_voter].isRegistered, "L'électeur doit être enregistré.");
        return voters[_voter].voteHistory;
    }
}
